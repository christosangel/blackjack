//
//▗▖   ▗▄▖            ▗▖     █            ▗▖
//▐▌   ▝▜▌            ▐▌     ▀            ▐▌
//▐▙█▙  ▐▌   ▟██▖ ▟██▖▐▌▟▛  ██   ▟██▖ ▟██▖▐▌▟▛
//▐▛ ▜▌ ▐▌   ▘▄▟▌▐▛  ▘▐▙█    █   ▘▄▟▌▐▛  ▘▐▙█
//▐▌ ▐▌ ▐▌  ▗█▀▜▌▐▌   ▐▛█▖   █  ▗█▀▜▌▐▌   ▐▛█▖
//▐█▄█▘ ▐▙▄ ▐▙▄█▌▝█▄▄▌▐▌▝▙   █  ▐▙▄█▌▝█▄▄▌▐▌▝▙
//▝▘▀▘   ▀▀  ▀▀▝▘ ▝▀▀ ▝▘ ▀▘  █   ▀▀▝▘ ▝▀▀ ▝▘ ▀▘
//                         ▐█▛
//a solitaire card game written by Christos Angelopoulos, under GPL v2, Oct 2022
function showResult2(phrase,sum)
{
  let coin = document.querySelector('#prefCurrency').textContent;
  if ( document.querySelector('#prefCurrencyFormat').textContent === "true" ){
    document.querySelector('#result2').textContent = phrase + coin + sum;
  }else{
    document.querySelector('#result2').textContent = phrase + sum + coin ;
  }
}
function showTotal(totalSum)
{
  let coin = document.querySelector('#prefCurrency').textContent;
  if ( document.querySelector('#prefCurrencyFormat').textContent === "true" ){
  document.querySelector('#total').textContent = 'Total Cash: ' + coin + totalSum;
}else{
  document.querySelector('#total').textContent = 'Total Cash: ' + totalSum + coin ;
}

}
function about()
{
  document.getElementById("set").classList.add("inactive");
  document.getElementById("aboutDiv").classList.remove("inactive");
  document.getElementById("infoAboutBDiv").classList.remove("inactive");
}
function infoAboutReturn()
{
  document.getElementById("set").classList.remove("inactive");
  document.getElementById("infoDiv").classList.add("inactive");
  document.getElementById("aboutDiv").classList.add("inactive");
  document.getElementById("infoAboutBDiv").classList.add("inactive");
}
function info()
{
  document.getElementById("set").classList.add("inactive");
  document.getElementById("infoDiv").classList.remove("inactive");
  document.getElementById("infoAboutBDiv").classList.remove("inactive");
}

function reloadPreferedValues()
{
  document.body.style.background = document.querySelector("#prefBGColor").textContent;
  document.querySelector("#colorPick").value = document.querySelector("#prefBGColor").textContent;
  document.querySelector("#bossHide").style.background = document.querySelector("#prefBGColor").textContent;
  document.querySelector("#inSumInp").style.background = document.querySelector("#prefBGColor").textContent;
  document.querySelector("#currencySelect").style.background = document.querySelector("#prefBGColor").textContent;
  document.querySelector("#currencyFormat").style.background = document.querySelector("#prefBGColor").textContent;
  document.querySelector("#audioSp").style.background = document.querySelector("#prefBGColor").textContent;
  document.querySelector("#inSumInp").value = document.querySelector("#prefInSum").textContent;
  document.querySelector("#bossHide").value = document.querySelector("#prefBoss").textContent;
  document.querySelector("#currencySelect").value  =  document.querySelector("#prefCurrency").textContent;
  document.querySelector("#currencyFormat").value = document.querySelector("#prefCurrencyFormat").textContent;


  if (document.querySelector("#audioBool").textContent === 'true'){
    document.querySelector("#toggleAudio").checked = true;
  }else{
    document.querySelector("#toggleAudio").checked  = false;
  };


}
function resetValues()
{
  document.querySelector("#prefBGColor").textContent = document.querySelector("#colorPick").value;
  if ( document.querySelector("#toggleAudio").checked === true ){
    document.querySelector("#audioBool").textContent = 'true';
  }else{
    document.querySelector("#audioBool").textContent = 'false';
  };
  document.querySelector("#prefBoss").textContent = document.querySelector("#bossHide").value;
  document.querySelector("#prefInSum").textContent = document.querySelector("#inSumInp").value;
  document.querySelector('#form1').max = document.querySelector("#prefInSum").textContent;
  document.querySelector("#prefCurrency").textContent = document.querySelector("#currencySelect").value;
  document.querySelector("#prefCurrencyFormat").textContent = document.querySelector("#currencyFormat").value;
  totalSum = Number(document.querySelector("#prefInSum").innerHTML.match(/\d+/g)[0]);
  showTotal(totalSum);
  document.getElementById("result2").innerHTML = document.getElementById("result2").innerHTML.replace(/€|\$|￡|￥|₽|₹,₩|L|₱|ƒ|₿|Ξ/, document.querySelector("#prefCurrency").textContent)
  document.getElementById("placedBet").innerHTML = document.getElementById("placedBet").innerHTML.replace(/€|\$|￡|￥|₽|₹,₩|L|₱|ƒ|₿|Ξ/, document.querySelector("#prefCurrency").textContent);
  for (var i = 0; i < 8; i++) {
    if ( Number(document.querySelector("#prefSet").textContent.match(i)) === i ){

      for (var x = 1; x <= document.querySelector("#setdealer").children.length; x++) {
        document.querySelector("#setdealer > img:nth-child(" + x + ")").src = document.querySelector("#setdealer > img:nth-child(" + x + ")").src.replace(/.*png.../, document.querySelector("#prefSet").textContent)
      };
      for (var x = 1; x <= document.querySelector("#setplayer").children.length; x++) {
        document.querySelector("#setplayer > img:nth-child(" + x + ")").src = document.querySelector("#setplayer > img:nth-child(" + x + ")").src.replace(/.*png.../, document.querySelector("#prefSet").textContent)
      };

      document.querySelector('#deckPrev1').src = document.querySelector('#prefSet').textContent + '0-11.png';
      document.querySelector(".framedS").classList.remove('framedS');
      document.getElementById('deckSetFr' + i).classList.add('framedS');
    };
    if ( Number(document.querySelector("#prefDeck").textContent.match(i)) === i ){
      document.getElementById('deck').src = document.querySelector("#prefDeck").textContent;
      document.getElementById('deckPrev0').src = document.querySelector('#prefDeck').textContent;
      document.querySelector(".framed").classList.remove('framed');
      document.getElementById('deckColFr' + i).classList.add('framed');
      if ( document.querySelector('#set0').children.length === 3 ){
        document.getElementById('dealerClosed').src = document.querySelector("#prefDeck").textContent;
      };
      if ( document.querySelector('#set1').children.length === 3 ){
        document.getElementById('playerClosed').src = document.querySelector("#prefDeck").textContent;
      };
    };
  };

}

function playSound(mp3)
{
  audioBool =document.querySelector("#audioBool").textContent;
  if (audioBool === "true"){
    var audio =	document.createElement("audio");
    audio.type	= "audio/wav";
    audio.setAttribute("src",mp3);
    audio.setAttribute("autoplay", "true");
  }
}

function preferences()
{
  document.getElementById("set").classList.add("prefInactive");
  document.getElementById("preferencePrompt").classList.remove("prefInactive");

}

function cardsClosed()
{
  let defDeck = document.querySelector('#prefDeck').textContent;
  document.querySelector('#deck').src = defDeck;
  document.querySelector('#dealerClosed').src = defDeck;
  document.querySelector('#playerClosed').src = defDeck;
}

function hitPlayer()
{
  let i = document.querySelector('#setplayer').childElementCount;
  if( i === 1 ){
    document.querySelector('#playerClosed').remove();
  }
  name=draw(cards,drawn);
  var player = document.createElement("img");
  document.getElementById("setplayer").appendChild(player);
  player.classList.add("player");
  player.src = document.querySelector('#prefSet').textContent + name + '.png';
  addup("#setplayer","playerCount","Player : ");
  playerScore = Number(document.querySelector("#playerCount").innerText.replace(/.*: /, ''));
  sum = Number(document.querySelector("#placedBet").innerHTML.match(/\d+/g)[0]);
  if (playerScore > 21) {
    LOSE("BUSTED! Player lost ",sum);
  };
  fiveCardsWin = Number(document.querySelector("#setplayer").childElementCount);
  if ((fiveCardsWin === 5)&&(playerScore <= 21)){
    WIN("WOW, FIVE CARDS! Player won ",sum);
    playSound("mp3/wow.mp3");
  };
  if ((fiveCardsWin === 2)&&(playerScore === 21)){
    sum = Math.ceil(3*sum/2);
    WIN("BLACKJACK! Player won ",sum);
    playSound("mp3/wow.mp3");
  };
  if ((playerScore === 21)&&(fiveCardsWin !== 2)) {
    WIN("WOW, 21! Player won ",sum);
    playSound("mp3/wow.mp3");
  };
}

function gameOver(totalSum,hand)
{
  let j = document.querySelector('#set0').childElementCount;
  if( j > 2 ){
    document.querySelector('#dealerClosed').remove();
  }
  let i = document.querySelector('#set1').childElementCount;
 if( i > 2 ){
   document.querySelector('#playerClosed').remove();
 }
  hand--;
  var tilt = document.createElement('img');
  tilt.id ='tilt';
  tilt.src = document.querySelector("#prefSet").textContent + 'joker.png';
  document.getElementById("setdeck").appendChild(tilt);
  let coin = document.querySelector('#prefCurrency').textContent;
  if (totalSum>100){
    playSound("mp3/game_win.mp3");
    if ( document.querySelector('#prefCurrencyFormat').textContent === "true" ){
      document.querySelector('#total').textContent = "Well done, you accumulated " + coin + totalSum  + " in "+hand+" hands. Play again?";
    }else{
      document.querySelector('#total').textContent = "Well done, you accumulated " + totalSum  + coin +" in "+hand+" hands. Play again?";
    }
  }else{
    playSound("mp3/game_lose.mp3");
    if ( document.querySelector('#prefCurrencyFormat').textContent === "true" ){
      document.querySelector('#total').textContent = "Nice job, you ended up with " + coin + totalSum  + " in "+hand+" hands. Play again?";
    }else{
      document.querySelector('#total').textContent = "Nice job, you ended up with " + totalSum  + coin +" in "+hand+" hands. Play again?";
    }
  };
  //var end = document.createElement("p");
  //end.id='end';
  //document.getElementById("set1").appendChild(end);
  //end.textContent ='GAME OVER';
  document.getElementById("end").classList.remove('inactive');
  document.querySelector('#setdealer').classList.add("inactive");
  document.querySelector('#setplayer').classList.add("inactive");
  document.querySelector('#hit').classList.add("inactive");
  document.querySelector('#stand').classList.add("inactive");
  document.querySelector('#bet').classList.add("inactive");
  document.querySelector('#setButtons').classList.add("inactive");
  document.querySelector('#continew').classList.add("inactive");
  document.querySelector('#placedBet').classList.add("inactive");
  document.querySelector('#result1').classList.add("inactive");
  document.querySelector('#result2').classList.add("inactive");
  document.querySelector('#form0').classList.add("inactive");
  document.querySelector('#form1').classList.add("inactive");
  document.querySelector('#playerCount').classList.add("inactive");
  document.querySelector('#dealerCount').classList.add("inactive");
  var prompt = document.createElement("div");
  prompt.id='prompt';
  document.getElementById("setButtons").appendChild(prompt);
  var yes = document.createElement("button");
  yes.id='yes';
  document.getElementById("prompt").appendChild(yes);
  document.querySelector('#yes').textContent = 'Yup!'
  yes.addEventListener('click', () =>{
    document.getElementById("tilt").remove();
    document.getElementById("end").classList.add('inactive');
    document.getElementById("prompt").remove();
    document.querySelector('#setdealer').classList.remove("inactive");
    document.querySelector('#setplayer').classList.remove("inactive");
    var playerClosed = document.createElement("img");
    playerClosed.id ='playerClosed';
    document.getElementById("set1").appendChild(playerClosed);
    playerClosed.src = document.querySelector('#prefDeck').textContent;
    var dealerClosed = document.createElement("img");
    dealerClosed.id ='dealerClosed';
    document.getElementById("set0").appendChild(dealerClosed);
    dealerClosed.src = document.querySelector('#prefDeck').textContent;
    document.querySelector('#bet').classList.remove("inactive");
    document.querySelector('#setButtons').classList.remove("inactive");
    document.querySelector('#result1').classList.remove("inactive");
    document.querySelector('#result2').classList.remove("inactive");
    document.querySelector('#placedBet').classList.remove("inactive");
    document.querySelector('#form1').max = document.querySelector("#prefInSum").textContent;
    document.querySelector('#form1').value = '1';
    document.querySelector('#form0').classList.remove("inactive");
    document.querySelector('#form1').classList.remove("inactive");
    document.querySelector('#playerCount').classList.remove("inactive");
    document.querySelector('#dealerCount').classList.remove("inactive");
    let totalSum = document.querySelector('#prefInSum').textContent;
    showTotal(totalSum);

    document.querySelector('#result1').textContent =  "Hand: 1";

    document.querySelector('#result2').textContent =  'Place your bet & click on the Bet button. Good luck!';
  })
}

function nameSum(event)
{
  var y = event.deltaY;
  totalSum = Number(document.querySelector("#total").innerHTML.match(/\d+/g)[0]);
  var sum = Number(document.querySelector("#placedBet").innerHTML.match(/\d+/g)[0]);
  if( y > 0 ){
    if ( sum < totalSum ){
      sum += 1;
      if ( document.querySelector('#prefCurrencyFormat').textContent === 'true' ){
        document.querySelector('#placedBet').textContent = document.querySelector('#prefCurrency').textContent + sum}
      else{ document.getElementById("placedBet").innerHTML = sum + document.querySelector('#prefCurrency').textContent;}
      document.getElementById("form1").value = sum;
    }
  }else{
    if ( sum > 0 ){
      sum -= 1;
      if ( document.querySelector('#prefCurrencyFormat').textContent === 'true' ){
        document.getElementById("placedBet").innerHTML = document.getElementById("prefCurrency").textContent + sum;
      }else{
        document.getElementById("placedBet").innerHTML = sum + document.getElementById("prefCurrency").textContent ;
      }
      document.getElementById("form1").value = sum;
    }
  };
}

function WIN(phrase,sum)
{
  playSound("mp3/win.mp3");
  document.querySelector('#hit').classList.add("inactive");
  document.querySelector('#stand').classList.add("inactive");
  document.querySelector('#hit').classList.add("inactive");
  document.querySelector('#continew').classList.remove("inactive");
  //sum = Number(document.querySelector('#placedBet').innerText);
  //phrase = phrase + sum;
  showResult2(phrase,sum);
  totalSum = Number(document.querySelector("#total").innerHTML.match(/\d+/g)[0]);
  totalSum +=sum;
  showTotal(totalSum);
}
function LOSE(phrase,sum)
{
  playSound("mp3/lose.mp3");
  document.querySelector('#hit').classList.add("inactive");
  document.querySelector('#stand').classList.add("inactive");
  document.querySelector('#hit').classList.add("inactive");
  document.querySelector('#continew').classList.remove("inactive");
  //sum = Number(document.querySelector('#placedBet').innerText);
  //phrase = phrase + sum;
  showResult2(phrase,sum);
  totalSum = Number(document.querySelector("#total").innerHTML.match(/\d+/g)[0]);
  totalSum -= sum;
  showTotal(totalSum);
}

function addup(parent,countId,countString)
{
  const cardsToAdd =[];
  let ace =0;
  let iMax = document.querySelector(parent).childElementCount;
  for(let i = 0; i < iMax; i++){
    let selector = parent + " > img:nth-child("+ (i+1)+")";
    cardsToAdd[i] = Number( document.querySelector(selector).src.replace(/.*-/, '').replace(/\.png/, ''));
    if ( cardsToAdd[i]> 10 ){ cardsToAdd[i] = 10};
    if ( cardsToAdd[i] === 1 ){
      cardsToAdd[i] = 11;
      ace++;
    }
  }
 let sum = 0;
 for (let x = 0;x < cardsToAdd.length;x++){
   sum +=Number(cardsToAdd[x]);
   if((sum>21)&&(ace>3)) {
      sum = sum - 10;
      ace--;
    }
   if((sum>21)&&(ace>2)) {
      sum = sum - 10;
      ace--;
    }
    if((sum>21)&&(ace>1)) {
     sum = sum - 10;
     ace--;
    }
     if((sum>21)&&(ace>0)) {
      sum = sum - 10;
      ace--;
    }
  }
 document.getElementById(countId).textContent = countString+sum;
}

function draw(cards,drawn)
{
  let loop = true;
  while(loop===true){
    rand = Math.ceil( Math.random()*(52)) - 1;
    name = cards[rand];
    let double = false;
    for ( zz = 0; zz < drawn.length;zz++){

      if ( drawn[zz] === name ){
        double = true;
        loop = true;
      }
    }
    if (double === false){
      drawn.push(name);
      loop = false;
      return name;
    }
    if (drawn.length === 52){
      alert("🂡 The deck will be reshuffled!");
      //let handText = document.querySelector('#result1').textContent;
      //document.querySelector('#result1').textContent = 'Deck reshuffled/' + handText;
      for (let l = 0; l<drawn.length;l++){
      drawn.pop();
      }
      loop=false;
      return name;
    }
  }
}

function dealerPlay(cards,drawn,lowLimit)
{

  sum = 0;
  while(sum<lowLimit){
    name = draw(cards,drawn);
    sum += name;
    var dealer = document.createElement("img");
    dealer.classList.add("dealer");
    document.getElementById("setdealer").appendChild(dealer);
    dealer.src = document.querySelector('#prefSet').textContent + name + '.png';
    addup("#setdealer","dealerCount","Dealer: ");
  }
}
const cards=[];
const drawn=[];
let name;
let i = 0;
for (let y = 0;y < 4;y++){
  for (let x = 1;x <  14;x++){
    let name = y+"-"+x;
    cards[i] = name;
    i++;
  }
}

function BET()
{

  document.querySelector('#form1').classList.add("inactive");
  document.querySelector('#form0').classList.add("inactive");
  document.querySelector('#bet').classList.add("inactive");
  document.querySelector('#setButtons').classList.add("inactive");
  document.querySelector('#placedBet').classList.add("inactive");
  document.querySelector('#hit').classList.remove("inactive");
  document.querySelector('#stand').classList.remove("inactive");
  var  sum = Number(document.querySelector("#placedBet").innerHTML.match(/\d+/g)[0]);

  showResult2('You bet ',sum);
  hitPlayer();
}

function CONTINUE()
{
  let i = document.querySelector('#set0').childElementCount;
  if( i > 2 ){
    document.querySelector('#dealerClosed').remove();
  }
  totalSum = Number(document.querySelector("#total").innerHTML.match(/\d+/g)[0]);
  document.querySelector('#form1').max = totalSum;
  document.querySelector('#form1').value = '1';
  document.querySelector('#form1').classList.remove("inactive");
  document.querySelector('#form0').classList.remove("inactive");
  document.querySelector('#continew').classList.add("inactive")
  document.querySelector("#setdealer").remove();
  document.querySelector("#setplayer").remove();
  var setdealer = document.createElement("div");
  setdealer.id = "setdealer";
  document.getElementById("set0").appendChild(setdealer);
  var setplayer = document.createElement("div");
  setplayer.id = "setplayer"
  document.getElementById("set1").appendChild(setplayer);
  var dealer = document.createElement("img");

  dealer.classList.add("dealer");
  document.getElementById("setdealer").appendChild(dealer);
  name = draw(cards,drawn);
  dealer.src = document.querySelector('#prefSet').textContent + name + '.png';
  var dealerClosed = document.createElement("img");
  dealerClosed.id ='dealerClosed';
  document.getElementById("set0").appendChild(dealerClosed);
  dealerClosed.src = document.querySelector('#prefDeck').textContent;
  var player = document.createElement("img");
  player.classList.add("player");
  document.getElementById("setplayer").appendChild(player);
  name = draw(cards,drawn);
  player.src = document.querySelector('#prefSet').textContent + name + '.png';
  var playerClosed = document.createElement("img");
  playerClosed.id ='playerClosed';
  document.getElementById("set1").appendChild(playerClosed);
  playerClosed.src = document.querySelector('#prefDeck').textContent;
  document.querySelector('#bet').classList.remove("inactive");
  document.querySelector('#setButtons').classList.remove("inactive");
  document.querySelector('#placedBet').classList.remove("inactive");
  document.querySelector('#playerCount').textContent = 'Player: 0 ';
  document.querySelector('#dealerCount').textContent = 'Dealer: 0 ';
  document.querySelector('#result2').textContent = 'Place your bet & click on the Bet button. Good luck!'
  if ( document.querySelector('#prefCurrencyFormat').textContent === "true" ){
    document.querySelector('#placedBet').textContent = document.querySelector('#prefCurrency').textContent + '1';
  }else{
    document.querySelector('#placedBet').textContent = '1' + document.querySelector('#prefCurrency').textContent ;
  };
  var hand = Number(document.querySelector("#result1").textContent.replace(/.*: /,''));
  hand++;
  document.querySelector("#result1").textContent = "Hand: " + hand;
  if(( hand > 10 )||(totalSum <= 0)) { gameOver(totalSum,hand); }
}

function keyEvent(event)
{
  var keyDown = event.key;
  if ( keyDown === document.querySelector('#prefBoss').textContent) {
    if ( document.querySelector("#set").classList.contains("bossInactive") === false ){
      document.querySelector("#set").classList.add("bossInactive");
      document.querySelector("#setSet").classList.add("bossInactive");
      document.querySelector("#infoDiv").classList.add("bossInactive");
      document.querySelector("#aboutDiv").classList.add("bossInactive");
      document.querySelector("#infoAboutBDiv").classList.add("bossInactive");
    }else{
      document.querySelector("#set").classList.remove("bossInactive");
      document.querySelector("#setSet").classList.remove("bossInactive");
      document.querySelector("#infoDiv").classList.remove("bossInactive");
      document.querySelector("#aboutDiv").classList.remove("bossInactive");
      document.querySelector("#infoAboutBDiv").classList.remove("bossInactive");
    }
  }
}
///////////////////////////////////////

var prefered = document.createElement("div");
prefered.id = 'prefered'
prefered.classList.add('inactive');
document.body.appendChild(prefered);
var preferedObjs=[{"name": 'prefBGColor',"textContent": '#2D452F'},
{"name": 'prefSet',"textContent": 'png/3/'},
{"name": 'prefDeck',"textContent": 'png/back/back5.png'},
{"name": 'audioBool',"textContent": 'true'},
{"name": 'trophyBool',"textContent": 'true'},
{"name": 'prefInSum',"textContent": '100'},
{"name": 'prefCurrency',"textContent": '€'},
{"name": 'prefCurrencyFormat',"textContent": 'true'},
{"name": 'prefAnte',"textContent": '0'},
{"name": 'prefAnteRaise',"textContent": '0'},
{"name": 'prefBoss',"textContent": 'z'}];
preferedObjs.forEach((pref,i) =>{
  let y = pref.name;
  y = document.createElement('p');
  y.id = pref.name;
  y.textContent = pref.textContent;
  document.getElementById("prefered").appendChild(y);
});
document.body.style.background = prefBGColor.textContent;
document.addEventListener('keydown', keyEvent);

var deck = document.createElement("img");
deck.id="deck";
document.getElementById("setdeck").appendChild(deck);
deck.src =prefDeck.textContent;
var dealer = document.createElement("img");
dealer.classList.add("dealer");
document.getElementById("setdealer").appendChild(dealer);
name = draw(cards,drawn);
dealer.src = prefSet.textContent + name + '.png';
var dealerClosed = document.createElement("img");
dealerClosed.id ='dealerClosed';
document.getElementById("set0").appendChild(dealerClosed);
dealerClosed.src = prefDeck.textContent;

var player = document.createElement("img");
player.classList.add("player");
document.getElementById("setplayer").appendChild(player);
name = draw(cards,drawn);
player.src = prefSet.textContent + name + '.png';

var playerClosed = document.createElement("img");
playerClosed.id ='playerClosed';
document.getElementById("set1").appendChild(playerClosed);
playerClosed.src = prefDeck.textContent;

var result1 = document.createElement("p");
result1.id='result1';
document.getElementById("results").appendChild(result1);
document.querySelector('#result1').textContent = 'Hand: 1';
var total = document.createElement("p");
total.id='total';
document.getElementById("results").appendChild(total);
let totalSum = document.querySelector("#prefInSum").textContent;
showTotal(totalSum);
var result2 = document.createElement("p");
result2.id='result2';
document.getElementById("results").appendChild(result2);
document.querySelector('#result2').textContent = 'Place your bet & click on the Bet button. Good luck!';
if ( document.querySelector('#prefCurrencyFormat').textContent === "true" ){
  document.querySelector('#placedBet').textContent = document.querySelector('#prefCurrency').textContent + '1';
}else{
  document.querySelector('#placedBet').textContent = '1' + document.querySelector('#prefCurrency').textContent ;
}

var dealerCount = document.createElement("p");
dealerCount.id=("dealerCount");
document.getElementById("counts").appendChild(dealerCount);
dealerCount.textContent ='Dealer: ';
var playerCount = document.createElement("p");
playerCount.id=("playerCount");
document.getElementById("counts").appendChild(playerCount);
playerCount.textContent ='Player: ';
addup("#setplayer","playerCount","Player: ");
addup("#setdealer","dealerCount","Dealer: ");
var continew = document.createElement("button");
continew.id="continew";
continew.classList.add("inactive");
document.getElementById("setButtons").appendChild(continew);
document.querySelector("#continew").textContent = "Continue";
continew.addEventListener('click', () =>{ CONTINUE();})
var stand = document.createElement("button");
stand.id="stand";
stand.classList.add("inactive");
document.getElementById("setButtons2").appendChild(stand);
document.querySelector("#stand").textContent = "Stand";
stand.addEventListener('click', () =>{

  document.querySelector('#dealerClosed').remove();

  sum = Number(document.querySelector("#placedBet").innerHTML.match(/\d+/g)[0]);
  lowLimit = Number(document.querySelector("#playerCount").innerText.replace(/.*: /, ''));
  dealerScore = Number(document.querySelector("#dealerCount").innerText.replace(/.*: /, ''));
  while (dealerScore<lowLimit){
    name=draw(cards,drawn);
    var dealer = document.createElement("img");
    document.getElementById("setdealer").appendChild(dealer);
    dealer.classList.add("dealer");
    dealer.src = prefSet.textContent + name + '.png';
    addup("#setdealer","dealerCount","Dealer: ");
    dealerScore = Number(document.querySelector("#dealerCount").innerText.replace(/.*: /, ''));
  }
  if (dealerScore>21) {
    WIN("Dealer went BUST! Player won ",sum);
  }
  else if (dealerScore=>lowLimit) {
    LOSE("Player lost ",sum);
  }
  else {
    WIN("Player won ",sum)
  }
});

var hit = document.createElement("button");
hit.id="hit";
hit.classList.add("inactive");
document.getElementById("setButtons2").appendChild(hit);
document.querySelector("#hit").textContent = "Hit";
hit.addEventListener('click', () =>{
hitPlayer();
})
////////////////////////// build prefDivs in setSet
var promptElements = [{"name": 'preferencePrompt',"parent": "setSet","tag": "div"},
  {"name": 'deckPreviewDiv',"parent": "preferencePrompt","tag": "div"},
  {"name": 'deckPrev0',"parent": "deckPreviewDiv","tag": "img", "src": "#prefDeck"},
  {"name": 'deckPrev1',"parent": "deckPreviewDiv","tag": "img", "src": "#prefDeck"},
  {"name": 'prefBackDiv',"parent": "preferencePrompt","tag": "div"},
  {"name": 'bossKeys',"parent": "preferencePrompt","tag": "div"},
  {"name": 'deckSetDiv',"parent": "preferencePrompt","tag": "div"},
  {"name": 'deckColor',"parent": "preferencePrompt","tag": "div"},
  {"name": 'inSumDiv',"parent": "preferencePrompt","tag": "div"},
  {"name": 'currencyDiv',"parent": "preferencePrompt","tag": "div"},
  {"name": 'boolPrompt',"parent": "preferencePrompt","tag": "div"},
  {"name": 'buttonDiv',"parent": "preferencePrompt","tag": "div"},
  {"name": 'colorForm',"parent": "prefBackDiv","tag": "form"},
  {"name": "colorLabel","parent": "colorForm","tag": "p", "text": 'Select Background color:'},
  {"name": 'colorPick',"parent": "colorForm","tag": "input"},
  {"name": 'hideKey',"parent": "bossKeys","tag": "p", "text": 'Hide Key:'},
  {"name": 'bossHide',"parent": "bossKeys","tag": "input"},
  {"name": 'deckSetP',"parent": "deckSetDiv","tag": "p", "text": 'Select Deck:'},
  {"name": 'deckColP',"parent": "deckColor","tag": "p", "text": 'Select Back:'},
  {"name": 'inSumP',"parent": "inSumDiv","tag": "p", "text": 'Initial Sum:'},
  {"name": 'inSumInp',"parent": "inSumDiv","tag": "input"},
  {"name": 'currencyP0',"parent": "currencyDiv","tag": "p", "text": 'Currency:'},
  {"name": 'currencySelect',"parent": "currencyDiv","tag": "select"},
  {"name": 'currencyP1',"parent": "currencyDiv","tag": "p", "text": 'Format:'},
  {"name": 'currencyFormat',"parent": "currencyDiv","tag": "select"},
  {"name": 'audioLabel',"parent": "boolPrompt","tag": "label", "text": 'Sounds:'},
  {"name": 'toggleAudio',"parent": "audioLabel","tag": "input"},
  {"name": 'audioSp',"parent": "audioLabel","tag": "span"},
  {"name": 'saveSettings',"parent": "buttonDiv","tag": "button"},
  {"name": 'loadSettings',"parent": "buttonDiv","tag": "button"},
  {"name": 'done',"parent": "buttonDiv","tag": "button"}];
promptElements.forEach((elem, i) => {
  let parentDiv = "#" + elem.parent;
  let y = elem.name;
  y = document.createElement(elem.tag);
  y.id = elem.name;
  if (( elem.tag === 'p' )||( elem.tag === 'label' )) {y.textContent = elem.text};
  if ( elem.tag === 'img' ) {let x = document.querySelector(elem.src).textContent; y.src = x};
  document.querySelector(parentDiv).appendChild(y);
});
var cardSets= ['0', '1', '2', '3', '4', '5', '6', '7'];
cardSets.forEach((cardSet,i) =>{
  let parentDiv = 'deckSetFr' + cardSet;
  parentDiv = document.createElement('div');
  parentDiv.id = 'deckSetFr' + cardSet;
  document.getElementById('deckSetDiv').appendChild(parentDiv);
  let y = 'deckSet' + cardSet;
  y = document.createElement('img');
  y.id = 'deckSet' + cardSet;
  y.src = 'png/' + cardSet + '/' + '0-11.png';
  document.getElementById('deckSetFr' + cardSet).appendChild(y);
  y.addEventListener('mouseover', () =>{
    deckPrev1.src = y.src;
  });
  y.addEventListener('mouseout', () =>{
    deckPrev1.src = document.querySelector('#prefSet').textContent + '0-11.png';
  });
  if (document.querySelector("#prefSet").textContent === 'png/' + cardSet + '/' ){
    parentDiv.classList.add('framedS');
  };
  y.addEventListener('click', () =>{
    document.querySelector("#prefSet").textContent = 'png/' + cardSet + '/';
    deckPrev1.src = document.querySelector("#prefSet").textContent +"0-11.png";
//    if ( String(document.querySelector('#CARDP').src.match('back')) !== "back" ){
//      document.querySelector('#CARDP').src = document.querySelector('#CARDP').src.replace(/.*png\/./, 'png/' + cardSet + '/')
//    };
//    if ( String(document.querySelector('#CARD0').src.match('back')) !== "back" ){
//      document.querySelector('#CARD0').src = document.querySelector('#CARD0').src.replace(/.*png\/./, 'png/' + cardSet + '/')
//      document.querySelector('#CARD1').src = document.querySelector('#CARD1').src.replace(/.*png\/./, 'png/' + cardSet + '/')
//    };
    document.querySelector(".framedS").classList.remove('framedS');
    parentDiv.classList.add('framedS');
  });
});
var backSets = ['0', '1', '2', '3', '4', '5', '6', '7'];
backSets.forEach((backSet,i) =>{
  let parentDiv = 'deckColFr' + backSet;
  parentDiv = document.createElement('div');
  parentDiv.id = 'deckColFr' + backSet;
  document.getElementById('deckColor').appendChild(parentDiv);
  let y = 'deckCol' + backSet;
  y = document.createElement('img');
  y.id = 'deckCol' + backSet;
  y.src = 'png/back/back' + backSet + '.png';
  document.getElementById('deckColFr' + backSet).appendChild(y);
  if (document.querySelector("#prefDeck").textContent.replace(/.*\/png\/back\//, "png/back/") === 'png/back/back' + backSet + '.png' ){
    document.getElementById('deckColFr' + backSet).classList.add('framed');
  }
  y.addEventListener('mouseover', () =>{
    deckPrev0.src = y.src;

  });
  y.addEventListener('mouseout', () =>{
    deckPrev0.src = document.querySelector('#prefDeck').textContent;

  });
  y.addEventListener('click', () =>{
    document.querySelector("#prefDeck").textContent = y.src;

    //if ( String(document.querySelector('#CARDP').src.match('back')) === "back" ){
    //  document.querySelector('#CARDP').src = document.querySelector("#prefDeck").textContent;
    //};
    //if ( String(document.querySelector('#CARD0').src.match('back')) === "back" ){
    ////  document.querySelector('#CARD0').src = document.querySelector("#prefDeck").textContent;
    //  document.querySelector('#CARD1').src = document.querySelector("#prefDeck").textContent;
    //};

    deckPrev0.src = document.querySelector("#prefDeck").textContent;

    document.querySelector(".framed").classList.remove('framed');
    parentDiv.classList.add('framed');
  });
});
var coins = ['€', '$', '￡', '￥', '₽', '₹','₩', 'L', '₱', 'ƒ', '₿', 'Ξ'];
coins.forEach((coin, i) => {
  let y = coin;
  var z = new Option(y,y);
  currencySelect.appendChild(z);
});
var currrencyF0 = new Option('¤1','true');
var currencyF1 = new Option('1¤','false');
currencyFormat.appendChild(currrencyF0);
currencyFormat.appendChild(currencyF1);

colorPick.type = 'color';
colorPick.name = 'favColor';
bossHide.type = 'text';
bossHide.name = 'boss0';
bossHide.maxLength = "1";
inSumInp.type = 'number';
inSumInp.min = '100';
inSumInp.max = '10000';
inSumInp.step = "100";
toggleAudio.type = 'checkbox';
audioLabel.classList.add('p');
audioSp.classList.add('vchkbox');
reloadPreferedValues();
colorPick.addEventListener('input', () =>{
  resetValues();
  reloadPreferedValues();
});


saveSettings.textContent = "Save";
saveSettings.addEventListener('click', () =>{
  document.getElementById('done').click();
  document.getElementById('prefButton').click();
 var settingsData = {prefBGColor: document.querySelector("#prefBGColor").textContent,
  prefSet: document.querySelector("#prefSet").textContent,
  prefDeck: document.querySelector("#prefDeck").textContent,
  audioBool: document.querySelector("#audioBool").textContent,
  trophyBool: document.querySelector("#trophyBool").textContent,
  prefInSum: document.querySelector("#prefInSum").textContent,
  prefCurrency: document.querySelector("#prefCurrency").textContent,
  prefCurrencyFormat: document.querySelector("#prefCurrencyFormat").textContent,
  prefAnte: document.querySelector("#prefAnte").textContent,
  prefAnteRaise: document.querySelector("#prefAnteRaise").textContent,
  prefBoss: document.querySelector("#prefBoss").textContent};
  var a = document.createElement('a');
  a.id = "a";
  a.href = URL.createObjectURL(new Blob([JSON.stringify(settingsData, null, 2)], {type: "application/json" }));
  buttonDiv.appendChild(a);
  a.download = 'Bucket_Settings.json';
  a.click();
  a.remove();
  resetValues();
  document.getElementById("set").classList.remove("prefInactive");
  document.getElementById("preferencePrompt").classList.add("prefInactive");
});
loadSettings.textContent = "Load";
loadSettings.addEventListener('click', () =>{
  var input =  document.createElement('input');
  input.id = "input";
  input.type = 'file';
  input.click();
  input.addEventListener('input', () =>{
    let file = input.files[0];
    let fileReader = new FileReader();
    fileReader.readAsText(file);
    fileReader.onload = function(){
      const elData = JSON.parse(fileReader.result);
      document.querySelector("#prefBGColor").textContent = elData.prefBGColor;
      document.querySelector("#prefSet").textContent = elData.prefSet;
      document.querySelector("#prefDeck").textContent = elData.prefDeck;
      document.querySelector("#audioBool").textContent = elData.audioBool;
      document.querySelector("#trophyBool").textContent = elData.trophyBool;
      document.querySelector("#prefInSum").textContent = elData.prefInSum;
      document.querySelector("#prefCurrency").textContent = elData.prefCurrency;
      document.querySelector("#prefCurrencyFormat").textContent = elData.prefCurrencyFormat;
      document.querySelector("#prefBoss").textContent = elData.prefBoss;
      reloadPreferedValues();
      resetValues();
    };
    document.getElementById("set").classList.remove("prefInactive");
    document.getElementById("preferencePrompt").classList.add("prefInactive");
  });
});
done.textContent = 'Done';
done.addEventListener('click', () =>{
  //reloadPreferedValues();
  resetValues();
  document.getElementById("set").classList.remove("prefInactive");
  document.getElementById("preferencePrompt").classList.add("prefInactive");
});
document.getElementById("preferencePrompt").classList.add("prefInactive");
