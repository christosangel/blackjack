# blackjack
---

![0.png](png/0.png)

The known card game, written in javascript, where the computer is the dealer. 

---

## BASIC BLACKJACK RULES

These are the rules of this version of the game.

The object of a game of blackjack is to get a hand of cards that has a value of 21 without
going over 21 (which is called "busting"). The player plays against the dealer, who is also
trying to get to 21 without busting. 

A hand of blackjack normally goes like this:

*  The 52 card deck is shuffled (the player will be __alerted__ each time the deck is reshuffled).

![7.png](png/7.png)

*  The player and the dealer are dealt one open card and one closed card each.

*  The cards have a "value" equal to the number printed on a card, regardless of set. Kings, Queens and Jacks are all worth 10, and an Ace is worth 11, unless the player
would bust, in which case the Ace is worth 1.

*  If the player gets a card with a value of 10 and an ace, he has blackjack, he has automatically won the round and his bet is paid back 3:2.


![6.png](png/6.png)

*  In any other case, the player can keep on hitting, or choose to stand when he thinks it is apropriate.


*  If the player stands and hasn't busted, the dealer has to hit until he has a value greater than the player's, then stands. 

The values of the two hands are compared, and the higher value wins.

After 10 hands, the player is presented with his total cash, and is asked whether he wishes to play again.


---


## INSTRUCTIONS

* Download zip file pressing the download button above.

* Unzip

* Double-click on index.html file, to open the game in a browser tab.

---


* __Place your bet on the slider, or by scrolling on it__ .

![1.png](png/1.png)

* __Click on the Bet button__ to place your bet.




* __Click on the Hit button__ and the computer will deal you a card.


![2.png](png/2.png)

* __Click on the Stay button__ when you think you have enough cards.

![3.png](png/3.png)



   Now the dealer is playing. After the end of the deal,

* __Click on the Continue button__ to move on to the next hand.

![4.png](png/4.png)

---
* After 10 hands, you will be prompted to play again or not:


![5.png](png/5.png)

---

## Settings

*   By clicking on the __settings__ image 🛠, right next to the slide, the user can set the appearance of the game according to his liking.

![preferences.png](png/preferences.png)

The user can set 

*   the background color
* the deck of cards
* the back image of the deck
* Initial Sum
* Prefered currency and format (currency symbol before/after the sum)
* Key that hides/shows again the game (boss key 😉 )
* Sound On / Off

Clicking on the __Done__ button will bring the user back to the game.

Clicking on the __Save__ or __Load__ button, the user can save/load settings to/from a file.

---



**Enjoy!**

---

*The rules of this version were adjusted from 
[this source.](https://sourceforge.net/projects/flumblackjack/)*

---


_Special thanks to_ 

* _my friend  John Papadopoulos, who helped me a great deal with troubleshooting._

*  _the members of_  [https://forums.linuxmint.com](https://forums.linuxmint.com)
      
    _who, with their feedback and input helped shaping up this game._


---